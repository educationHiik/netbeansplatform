/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ship;

/**
 *
 * @author vaganovdv
 */
public class AbsctractShip
{

    // =========== Группа {Поля класса} 
    // Номер записи в баз данных 
    private long id;
    
    // модификатор доступа публичный
    // |
    private  String shipName;
    
    // модификатор доступа частный
    // |
    private  String shipType;
    
    
    
    // =========== Группа {действия (методы ) класса} 
    // Конструктор класса
    //    Нет возвращаемого значения
    //     |
    public   AbsctractShip ()
    {
         shipName = "Неизвестное имя";
         shipType = "Неизвестный тип";
    }
   
    
    public   AbsctractShip (String name)
    {
         shipName = name;
         shipType = "Неизвестный тип";
    }
    
    
      public   AbsctractShip (String name, String type)
    {
         shipName = name;
         shipType = type;
    }
    
      
     
      
    /**
     *  Служит для чтения значения поля  
     * 
     */
    //  доступ всем   тип возвращаемого значения         Имя метода 
    //  |                    |                             |
    public                  long                         getId          ()
    {
       // возврат значения для обработки в классе, который вызвал метод
       //  |  
        return id;
    }

    /**
     * Запись идентификатора корабля 
     */
    public void setId(long id)
    {
    //  обращение к полю или методу текущего класса
    //     
    //   |    
        this.id = id;
    }
      
    
    
    /**
     * @return the shipName
     */
    public String getShipName()
    {
        return shipName;
    }

    /**
     * @param shipName the shipName to set
     */
    public void setShipName(String shipName)
    {
        if (shipName != null && !shipName.isEmpty())
        {
            this.shipName = shipName;
        }   
        else
        {
            System.out.println("Ошибка: имя корабля не может быть пустым");
        }
    }

    /**
     * @return the shipType
     */
    public String getShipType()
    {
        return shipType;
    }

    /**
     * @param shipType the shipType to set
     */
    public void setShipType(String shipType)
    {
        this.shipType = shipType;
    }
    
}

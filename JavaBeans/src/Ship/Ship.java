/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ship;

/**
 *
 * @author vaganovdv
 */
public class Ship extends AbsctractShip
{
   public int cabinCount;
   public int decks;
   
    public Ship()
    {
    }

    public Ship(String name)
    {
        super(name);
    }

    public Ship(String name, String type)
    {
        super(name, type);
    }
    
}

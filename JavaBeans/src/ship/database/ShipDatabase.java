/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ship.database;

//      имя пакета  имя пакета 
//       |          /
import Ship.AbsctractShip;
import java.util.ArrayList;
import java.util.List;

/**
 * База данных для хранения кораблей
 * 
 * @author vaganovdv
 */
public class ShipDatabase
{

    // База данных кораблей в памяти
    // список        Тип данных, которые хранятся в списке  название списка
    // |              |                                      |
    List         <AbsctractShip>                        shipDatabase = new ArrayList<>();
    
    
    public ShipDatabase()
    {
    }
    
    
    /**
     * Функция создания начальной базы данных кораблей
     */
    public void createShipList()
    {
    
        AbsctractShip ship1 = new  AbsctractShip();
        ship1.setShipName("Вольный");
        ship1.setShipType("Крейсер");
        
        
        AbsctractShip ship2 = new  AbsctractShip();
        ship2.setShipName("Светлый");
        ship2.setShipType("Линкор");
        
        AbsctractShip ship3 = new  AbsctractShip();
        ship2.setShipName("");
        ship2.setShipType("Линкор");
        
    
        System.out.println("Имя :" + ship1.getShipName());
        System.out.println("Тип :" + ship1.getShipType());
        
        shipDatabase.add(ship1);
        shipDatabase.add(ship2);
        
        System.out.println("В базе данных :"+ shipDatabase.size() +" кораблей");
    }        
    
    
}
